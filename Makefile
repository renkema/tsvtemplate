
SHELL=/usr/bin/bash

tsvtemplate.pdf: tsvtemplate.tex tsvtemplate.sty tsvtemplate.doc
	luatex tsvtemplate.doc

package: tsvtemplate.pdf
	mkdir tsvtemplate
	cp tsvtemplate.{doc,tex,pdf,sty} README LICENSE tsvtemplate/
	tar czf tsvtemplate.tar.gz tsvtemplate
	rm -r tsvtemplate/

